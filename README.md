## Disable high volume warning on Sailfish OS

It is meant to be installed through [patchmanager3](https://openrepos.net/content/coderus/patchmanager-30).

---

Problem:  
It's the same warning one also gets on Android phones when listening on headphones (or anything pugged into the audio jack). It blocks raising the volume above a certain level until some sort of OK button is tapped.

Solution:  
This patch disables that by pretending that the warning has been acknowledged already (in /usr/share/lipstick-jolla-home-qt5/volumecontrol/VolumeControl.qml).

Disclaimer: be aware that doing this you remove a safety feature and could damage your hearing. Use at your own risk.

Original patch: Ancelad (this version contains no new code, just made compatible with the newest SailfishOS)
